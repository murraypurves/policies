# Puppet Cam

## Privacy policy

This is an camera app developed by Murray Purves.

### Data collected by the app

The application requests the minimal set of user permissions required for the application to function - these are access to the camera, microphone and device storage.

This app does collect any personally identifiable information; all data created during its use (image, video and audio recordings) is stored locally on your device in the camera gallery and app cache. These files can be deleted from the camera gallery and by clearing the app's data or uninstalling it.

Analytics are limited to those used by the app store to which you have downloaded this app, which track the number and region of those users installing it.
